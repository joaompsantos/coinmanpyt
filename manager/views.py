from django.shortcuts import render
from .models import Coin

def index(request):
    return render(request, 'manager/index.html')