# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('manager', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coin',
            name='year',
            field=models.IntegerField(default=datetime.datetime(2015, 12, 6, 16, 14, 17, 935645, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
