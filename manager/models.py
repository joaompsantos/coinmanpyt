from django.db import models
from django.utils import timezone

# Create your models here.

class Coin(models.Model):
   value    = models.IntegerField()
   country  = models.CharField(max_length=200)
   release_year    = models.IntegerField()
   link2pic = models.TextField()		  